import explore_dnb as dnb

# Ici vos fonctions dédiées aux interactions



def afficher_menu(titre, liste_options):
    tiret = "-"*len(titre)
    print(f"+-{tiret}-+")
    print(f"| {titre} |")
    print(f"+-{tiret}-+")
    for i,n in enumerate(liste_options):
        index = i+1
        print(f" {index} -> {n}")


def demander_nombre(message, borne_max):
    s = input(message )
    if s.isdecimal():
        i = int(s)
        if 1 <= i <= borne_max:
            return i
    return None


def menu(titre, liste_options):
    afficher_menu(titre,liste_options)
    n = len(liste_options)
    return demander_nombre(f"Entrez votre choix [1-{n}]: ", n)

def filtre_college(liste_resultats, nom, departement):
    """génère la sous-liste de liste_resultats, restreinte aux résultats du département donné et dont le nom du collège contient le nom passé en paramètre (en minuscule ou majuscule)

    Args:
        liste_resultats (list): une liste de résultats
        nom (str): un nom de collège (éventuellement incomplet)
        departement (int) : un numéro de département

    Returns:
        list: la sous-liste de liste_resultats, restreinte aux résultats du collège et du département recherchés
    """
    if not liste_resultats: 
        return None
    liste = []
    for tuple in liste_resultats:
        if nom == tuple[1] and tuple[2] == departement and dnb.taux_reussite(tuple) !=None: #Si le tuple est bien agencé(=sans erreur), et que l'année et le département correspond 
            liste.append(tuple)
    return liste

def total_admis_presents(liste_resultats,debut,fin):
    """calcule le nombre total de candidats admis et de candidats présents aux épreuves du DNB parmi les résultats de la liste passée en paramètre

    Args:
        liste_resultats (list): une liste de résultats

    Returns:
        tuple : un couple d'entiers contenant le nombre total de candidats admis et présents
    """
    if not liste_resultats:
        return None
    nb_total = 0
    nb_admis = 0
    for tuple in liste_resultats:
        if type(tuple[-1]) is int and type(tuple[-1]) is int and tuple[-1] >= 0 and tuple[-2] >= 0 and dnb.taux_reussite(tuple) != None and debut <= tuple[0] <= fin: #Si le tuple est bien agencé(=sans erreur)
            nb_total += tuple[-2] #augmentation du nombre total
            nb_admis += tuple[-1] #augmentation du nombre d'admis
    return nb_admis,nb_total
""
def verif(comparaison):
    while True:
        try:  
            debut = int(input('Veuillez entrer l\'année du début de votre période : '))
            if comparaison[0] <= debut <= comparaison[1]:
                fin = int(input('Veuillez entrer l\'année de la fin de votre période : '))
                if comparaison[0] <= fin <= comparaison[1]:
                    break
                else:
                    print('Les règles !!!!!')
            else:
                print('Les règles !!!!!!')
        except:
            print("Je ne sais pas quoi dire !!!!!!!!!!!!!")
    return debut,fin


def part_question(lst_session,college,liste_options2):
    anne_college = dnb.liste_sessions(lst_session)
    debut_periode = 0
    fin_periode = 0
    while True:  #Partie avec les question
        sur = False
        rep = menu(college[1], liste_options2)
        if rep is None:
            print("Cette option n'existe pas")
            print('')
        elif rep == 1:
            print("Vous avez choisi l'option : ", liste_options2[rep - 1])
            print('')
            print('Le meilleur taux de réussite du collège {} est : {}%'.format(college[1],dnb.meilleur_taux_reussite(lst_session)))
            print('')
            print('Le pire taux de réussite du collège {} est : {}%'.format(college[1],dnb.pire_taux_reussite(lst_session)))
            print('')
        elif rep == 2:
            print("Vous avez choisi l'option : ", liste_options2[rep - 1])
            print('')
            print('Voici choisir une période compris entre :[{}-{}] '.format(anne_college[0],anne_college[-1]))
            while True:
                debut_periode,fin_periode = verif([anne_college[0],anne_college[-1]])
                print('La .... est de : {} '.format(total_admis_presents(lst_session,debut_periode,fin_periode)))
                break
        else:
            break


# ici votre programme principal


def programme_principal():
    liste_options = ["Charger un fichier",
                     "Quitter"]
    liste_options2 =["Quel est le (Meilleur/Pire) taux ?",
                     "NB d'éléve admis selon une période (donnée)?",
                     "Quitter"]
    
    liste_college = []
    liste_session = []
    Menu = True
    while Menu: #Partie pour charger le fichier
        partie_college = False 
        partie_depp = False
        sur = False
        rep = menu("MENU DE MON APPLICATION", liste_options)
        if rep is None:
            print("Cette option n'existe pas")
            print('')
            print('')
        elif rep == 1:
            print("Vous avez choisi l'option : ", liste_options[rep - 1])
            try: #Charge le fichier
                name_fic = input('Entrer un nom de fichier : ')
                fichier = dnb.charger_resultats(name_fic)
                print('Très bien le fichier {} est chargé. '.format(name_fic))
                partie_college = True 
                partie_depp = True
            except: #Réessaye
                print('')
                print('Aye...')
                print('Désoler nous n\'avons pas trouver le fichier {}. '.format(name_fic))
                print('Veuillez entrer un nom de fichier valable !!!')
                print('')
                print('')

        else:
            Menu = False
            
        while partie_depp: #Partie pour choisir le département
            try:
                print('')
                num_dep = int(input('Entrer un numéro de département : '))
                partie_depp = False
                break
            except:
                print('Veuillez entrer un numéro de département !!!')
        while partie_college: #Partie pour choisir le collège
            liste_college = dnb.filtre_departement(fichier,num_dep)[:10]
            for i,college in enumerate(liste_college):
                print('')
                print('')
                print(i,college[1])
            try:
                print('')
                num_college = int(input('Veuillez rentrer un numéro correspondant au collège voulue : '))
                print('Très bien vous avez choisie le collège {} '.format(liste_college[num_college][1]))
                sur = input('Êtes-vous sur de votre choix ? [o-n]: ')
                if sur in ['o','O']:
                    print('Très bien')
                    sur = True
                    college_choisi = liste_college[num_college] #Permet d'avoir le tuple du collège
                    liste_session = filtre_college(fichier,college_choisi[1],num_dep) #Permet d'avoir tout les tuples du collège choisie
                    partie_college = False
                    break
                else:
                    print('Très bien ')
                    print('Mais !!!')
                    print('Veuillez choisir un collège !!!')
            except:
                print('Veuillez choisir un collège !!!')
            
        if sur:      
            print('')
            part_question(liste_session,college_choisi,liste_options2)
            print('Vous avez quitter ce menu')
            print('')

            
    print('Au revoir !!!')
    
    
    
    
    
    
programme_principal()
    
    

    
    
liste1 = [(2020, 'ALBERT SIDOISNE', 28, 134, 118),
          (2020, 'ANATOLE FRANCE', 28, 63, 47),
          (2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60),
          (2020, 'DE NERMONT - NOGENT', 28, 28, 27),
          (2020, 'EMILE ZOLA', 28, 103, 88),
          (2020, 'GILBERT COURTOIS', 28, 22, 18),
          (2020, 'MATHURIN REGNIER', 28, 152, 118)]

    
liste4 = [(2008, "JEANNE D'ARC", 45, 71, 67),
          (2012, "ALBERT SIDOISNE", 28, 98, 78),
          (2016, "JEAN MONNET", 37, 115, 109),
          (2020, 'ALBERT SIDOISNE', 28, 134, 118),
          (2020, 'MATHURIN REGNIER', 28, 152, 118),
          ]
