import explore_dnb as dnb

# -----------------------------------------------------------------------------------------------------
# fonctions de tests à compléter
# -----------------------------------------------------------------------------------------------------
#Rajouter des assert !!!!!!!
def test_taux_reussite():
    assert dnb.taux_reussite(dnb.resultat1) == 67/71*100
    assert dnb.taux_reussite(dnb.resultat2) == 78/98*100
    assert dnb.taux_reussite(()) is None
    assert dnb.taux_reussite(dnb.resultat4) is None
           
def test_meilleur():
    assert dnb.meilleur(dnb.resultat1, dnb.resultat2) == True
    assert dnb.meilleur(dnb.resultat1, dnb.resultat3) == False
    assert dnb.meilleur(dnb.resultat3,dnb.resultat4) is None
    assert dnb.meilleur(dnb.resultat1, ()) is None
    assert dnb.meilleur((), ()) is None



def test_meilleur_taux_reussite():
    assert dnb.meilleur_taux_reussite(dnb.liste2) == 27/28*100
    assert dnb.meilleur_taux_reussite(dnb.liste3) == 1.0*100
    assert dnb.meilleur_taux_reussite([])  == None
    assert dnb.meilleur_taux_reussite([(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléonarte", 45, -1, 1)]) == None
    
    


def test_pire_taux_reussite():
    assert dnb.pire_taux_reussite(dnb.liste1) == 47/63*100
    assert dnb.pire_taux_reussite(dnb.liste2) == 15/24*100
    assert dnb.pire_taux_reussite(dnb.liste4) == 118/152*100 
    assert dnb.pire_taux_reussite([]) == None
    assert dnb.pire_taux_reussite([(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléon", 45, 0, 0)]) == None



def test_total_admis_presents():
    assert dnb.total_admis_presents(dnb.liste1) == (476,576)
    assert dnb.total_admis_presents(dnb.liste2) == (922, 1111)
    assert dnb.total_admis_presents([]) == None
    assert dnb.total_admis_presents([(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléon", 45, 1, 1)]) == (1,1)
    


def test_filtre_session():
    assert dnb.filtre_session(dnb.liste4, 2020) == [(2020, 'ALBERT SIDOISNE', 28, 134, 118),(2020, 'MATHURIN REGNIER', 28, 152, 118)]
    assert dnb.filtre_session(dnb.liste4, 2018) == []
    assert dnb.filtre_session(dnb.liste4, '2018') == None

    
    
    
def test_filtre_departement():
    assert dnb.filtre_departement(dnb.liste1, 45) == []
    assert dnb.filtre_departement(dnb.liste4, 28) == [(2012, "ALBERT SIDOISNE", 28, 98, 78),(2020, 'ALBERT SIDOISNE', 28, 134, 118),(2020, 'MATHURIN REGNIER', 28, 152, 118)]


def test_filtre_college():
    assert dnb.filtre_college(dnb.liste1, 'EMILE', 45) == []
    assert dnb.filtre_college(dnb.liste1, 'NERMONT', 28) == [(2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60),(2020, 'DE NERMONT - NOGENT', 28, 28, 27)]
    

def test_taux_reussite_global():
    assert dnb.taux_reussite_global(dnb.liste1, 2018) == None
    assert dnb.taux_reussite_global(dnb.liste1, 2020) == 476/576*100
    assert dnb.taux_reussite_global([(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléon", 45, 0, 0)],2008) == None
    assert dnb.taux_reussite_global([], 2018) == None


def test_moyenne_taux_reussite_college():
    liste = [(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléon", 45, -1, 1)]
    assert dnb.moyenne_taux_reussite_college(dnb.liste1, 'ALBERT SIDOISNE', 28) == 118/134*100
    assert dnb.moyenne_taux_reussite_college(dnb.liste2, 'GILBERT COURTOIS', 28) == (18/22*100+15/24*100)/2
    assert dnb.moyenne_taux_reussite_college([], 'ALBERT SIDOISNE', 28) == None
    assert dnb.moyenne_taux_reussite_college(liste, 'Nap', 45) == None

def test_meilleur_college():
    liste = [(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléon", 45, -1, 1)]
    assert dnb.meilleur_college(dnb.liste1, 2018) == None
    assert dnb.meilleur_college(dnb.liste2, 2021) == ('JEAN MONNET', 28)
    assert dnb.meilleur_college(liste,2008) == None
    assert dnb.meilleur_college([],2008) == None



def test_liste_sessions():
    liste = [(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléon", 45, -1, 1)]
    assert dnb.liste_sessions([]) == []
    assert dnb.liste_sessions(dnb.liste2) == [2020, 2021]
    assert dnb.liste_sessions(liste) == []

    


def test_plus_longue_periode_amelioration():
    liste = [(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléon", 45, -1, 1)]
    assert dnb.plus_longe_periode_amelioration([]) == None
    assert dnb.plus_longe_periode_amelioration(liste) == None
    assert dnb.plus_longe_periode_amelioration(dnb.liste5) == (2013, 2017)
    assert dnb.plus_longe_periode_amelioration(dnb.liste1) == (2020, 2020)


def test_est_bien_triee():
    liste = [(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléon", 45, -1, 1)]
    assert dnb.est_bien_triee(dnb.liste1) == True
    assert dnb.est_bien_triee([]) == True
    assert dnb.est_bien_triee(liste) == False
    


def test_fusionner_resultats():
    liste = [(2008, "Napoléonarte", 45, 71, -3),(2008, "Napoléon", 45, -1, 1)]
    assert dnb.fusionner_resultats(dnb.liste1, dnb.liste2) == [(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ANATOLE FRANCE', 28, 63, 47), (2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60), (2020, 'DE NERMONT - NOGENT', 28, 28, 27), (2020, 'EMILE ZOLA', 28, 103, 88), (2020, 'GILBERT COURTOIS', 28, 22, 18), (2020, 'MATHURIN REGNIER', 28, 152, 118), (2021, 'DE BEAUMONT LES AUTELS', 28, 37, 34), (2021, 'DE NERMONT - CHATEAUDUN', 28, 71, 60), (2021, 'EMILE ZOLA', 28, 96, 85), (2021, 'GILBERT COURTOIS', 28, 24, 15), (2021, 'JEAN MONNET', 28, 97, 91), (2021, 'LA PAJOTTERIE', 28, 91, 72), (2021, 'ND - LA LOUPE', 28, 12, 9), (2021, 'PIERRE BROSSOLETTE', 28, 93, 70), (2021, 'SULLY', 28, 14, 10)]
    assert dnb.fusionner_resultats(dnb.liste1, []) == dnb.liste1
    assert dnb.fusionner_resultats(dnb.liste2, liste) == dnb.liste2

